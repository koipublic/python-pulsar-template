FROM public.ecr.aws/koireader/base-images:release.ubuntu20.python.pulsar2.8 as release

ENV VENV_PATH=/opt/venv \
    TINI_VERSION=v0.19.0
ENV PATH="$POETRY_PATH/bin:$VENV_PATH/bin:$PATH"

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini

RUN apt-get update \
    && apt-get install --no-install-recommends -yq libpq-dev \
    && chmod +x /tini \
    && adduser koireader \
    # cleanup
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


FROM public.ecr.aws/koireader/base-images:builder.ubuntu20.python.pulsar2.8 as poetry

WORKDIR /app

COPY poetry.lock pyproject.toml ./
RUN poetry export -f requirements.txt --without-hashes > requirements.txt \
    && pip install --no-cache-dir -r requirements.txt
# RUN poetry install --no-interaction --no-ansi --no-dev


FROM release as runtime

WORKDIR /app

ENV PATH="$VENV_PATH/bin:$PATH"

COPY --chown=koireader:koireader --from=poetry $VENV_PATH $VENV_PATH
COPY --chown=koireader:koireader . ./

USER root

RUN chown -R koireader:koireader /app \
  && chmod +x /app/health.sh

USER koireader

ENTRYPOINT [ "/tini", "--" ]
CMD [ "python", "-m", "main" ]
