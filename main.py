import os
import sys

import orjson

import src.utils.settings
from src.utils._logger import logger
from src.utils.pulsar_utils import PulsarClientSetup
from src.utils.sentry import sentry_kr

p_client = PulsarClientSetup()
producer = p_client.create_producer(os.getenv("TOPIC"))
error_producer = p_client.create_producer(os.getenv("ERROR_TOPIC"))
consumer = p_client.subscribe(
    os.getenv("CONSUMER-TOPIC"),
    subscription_name=os.getenv("SUBSCRIPTION_NAME"),
    unacked_messages_timeout_ms=25000,
)


def main():
    while True:
        logger.info(f"Waiting for msg..")
        msg = consumer.receive()
        data = msg.data()
        logger.debug(f"data type = {type(data)} data size = {sys.getsizeof(data)}")
        data = orjson.loads(data)
        logger.debug(f"{data.keys()}")
        try:
            # redelivery count > threshold would mean that the data is currently unprocessable.
            # So, no point in further retrying. Acknowledge the data
            if msg.redelivery_count() >= int(os.getenv("MAX_REDELIVERY_COUNT", 5)):
                redelivery_error(data, msg)
                continue

            # If redelivery cnt <= threshold, try to process as systems can fail because of many reasons
            # like network issue (DNS) or memory issue. These sometimes get solved automatically in next iterations
            # Or when pod changes

            # NOTE: DO YOUR PROCESSING STUFF HERE
            logger.info("Processing data ..")

        except Exception as main_err:
            sentry_kr.capExc(main_err)
            # Unknown exceptions should be acknowledged. No point in retrying.


# try:
#     a = 10
#     print(b)
# except Exception as err:
#     sentry_kr.capExc(err)

if __name__ == "__main__":
    main()
