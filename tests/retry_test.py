from tenacity import (
    retry,
    stop_after_attempt,
    stop_after_delay,
    wait_exponential,
    wait_fixed,
    wait_random,
)


def callback(test, *argv):
    print(test)


# @retry(stop=stop_after_delay(8) | stop_after_attempt(20), wait=wait_random(min=0.2, max=1))
@retry(
    stop=stop_after_delay(10) | stop_after_attempt(6),
    wait=wait_exponential(multiplier=0.3, min=0.8, max=5),
)
def retry_test():
    print("Test...")
    print(a)


retry_test()
