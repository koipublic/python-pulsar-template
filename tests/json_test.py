import json
from time import time

import orjson
import rapidjson
import ujson


def loads(data, t):
    st = time()
    for _ in range(100):
        # if t == "ujson":
        # ujson.loads(data)
        # elif t == "rapidjson":
        # rapidjson.loads(data)
        # else:
        # orjson.loads(data)
        json.loads(data)
    print(t, time() - st)


def dumps(data, t):
    st = time()
    for _ in range(100):
        if t == "ujson":
            ujson.dumps(data)
        elif t == "rapidjson":
            rapidjson.dumps(data)
        elif t == "orjson":
            orjson.dumps(data)
        else:
            json.dumps(data)
    print(t, time() - st)


with open("tjson.json", "r") as f:
    data = json.loads(f.read())
    print(type(data))
    dumps(data, "json")
    dumps(data, "ujson")
    dumps(data, "orjson")
    dumps(data, "rapidjson")
