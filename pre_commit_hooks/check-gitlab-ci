#!/bin/sh

GITLAB_SERVER="https://gitlab.com"
GITLAB_CI_FILE=".gitlab-ci.yml"

CURL_OPTIONS="--connect-timeout 2 --max-time 4"

for program in jq curl; do
  if ! command -v $program >/dev/null 2>&1; then
    echo >&2 "I require $program but it's not installed.  Aborting."
    exit 2
  fi
done

if ! [ -r "$GITLAB_CI_FILE" ]; then
  echo "No $GITLAB_CI_FILE found" >&2
  exit 3
fi

# Stringify content
response="$(jq -Rs '. | {"content": .}' < $GITLAB_CI_FILE |
  curl $CURL_OPTIONS -s --header "Content-Type: application/json" \
  "$GITLAB_SERVER/api/v4/ci/lint" \
  --data @-)"
if [ "$?" != 0 ]; then
  echo "check-gitlab-ci command failed abnormally" >&2
  exit 4
fi

status="$(echo "$response" | jq -r .status)"

if [ "$status" = 'valid' ]; then
  echo "$GITLAB_CI_FILE is valid"
else
  echo "$GITLAB_CI_FILE is invalid:" >&2
  echo "$response" |jq -r '.errors[]|.' >&2
  exit 1
fi