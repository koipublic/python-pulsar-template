# builtin imports
import atexit
import os
from typing import List, Optional, Union

# pypi imports
import orjson
from pulsar import Client, CompressionType, ConsumerType
from tenacity import retry, stop_after_attempt, stop_after_delay, wait_exponential

# project imports
from ._logger import logger

# from sentry_sdk import capture_exception as capex


class PulsarClientSetup:
    def __init__(self) -> None:
        self.pulsar_client = Client(os.getenv("PULSAR_URL"))

    def create_producer(
        self, topic: str, send_timeout_millis: int = 25000, name: Optional[str] = None
    ) -> None:
        return self.pulsar_client.create_producer(
            topic,
            producer_name=name,
            send_timeout_millis=send_timeout_millis,
            compression_type=CompressionType.LZ4,
        )

    def subscribe(
        self,
        topic: Union[str, List],
        subscription_name: str,
        unacked_messages_timeout_ms: int,
    ) -> None:
        return self.pulsar_client.subscribe(
            topic,
            subscription_name=subscription_name,
            consumer_type=ConsumerType.Shared,
            unacked_messages_timeout_ms=unacked_messages_timeout_ms,
            negative_ack_redelivery_delay_ms=2000,
        )

    @retry(
        stop=stop_after_delay(10) | stop_after_attempt(6),
        wait=wait_exponential(multiplier=0.3, min=0.8, max=5),
    )
    def produce(producer, data):
        producer.send(orjson.dumps(data).encode("utf-8"))
        logger.info("Data produced...")

    @retry(
        stop=stop_after_delay(10) | stop_after_attempt(6),
        wait=wait_exponential(multiplier=0.3, min=0.8, max=5),
    )
    def acknowledge(consumer, msg):
        consumer.acknowledge(msg)
        logger.info("Acked...")

    @retry(
        stop=stop_after_delay(10) | stop_after_attempt(6),
        wait=wait_exponential(multiplier=0.3, min=0.8, max=5),
    )
    def negative_acknowledge(consumer, msg):
        consumer.acknowledge(msg)
        logger.info("Negative Acknowledge...")
