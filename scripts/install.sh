#!/bin/bash

echo "Starting installation"

eval "$(conda shell.bash hook)"
echo "Activated conda"

ENV_NAME="python-pulsar-template"
conda create -n $ENV_NAME Python=3.8
conda activate $ENV_NAME
echo "Env activated"

# codeartifact login
aws codeartifact login --tool pip --repository document --domain koireader --domain-owner 158511266486 --region us-west-2

curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python
echo "Installed poetry"

# source $HOME/.poetry/env
~/.local/bin/
poetry install --no-interaction
echo "Poetry dependencies installed"

echo "Installing pre-commit and commit-msg hook"
pre-commit install
pre-commit install --hook-type commit-msg

echo "Setup done"
echo "Execute  'conda activate $ENV_NAME' "
